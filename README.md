# BmatExercise


## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.


## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Why choose the layout?
To allow the user operate everything within the same page. Makes it easy for the user to perform multiple operations within the same page

## What to do to improve user experience
1. Automatically insert record into database if there's no matching songs in the database.
