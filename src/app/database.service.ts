import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { from, Observable } from 'rxjs';
import { of } from 'rxjs/internal/observable/of';
import { delay } from 'rxjs/internal/operators';
import { concatMap } from 'rxjs/internal/operators';



@Injectable({
  providedIn: 'root'
})
export class DatabaseService {

  csvDB: any;

  constructor(private http: HttpClient) {
    this.csvDB = 'assets/sound_recordings.csv';
  }

  findAll() {
    return this.http.get(this.csvDB, { responseType: 'text' });
  }


  // Simulating record insertion into database 

  insert(data): Observable<any> {
    console.log(data);
    return from([{ success: true }])
          .pipe(concatMap(item => of(item).pipe(delay(1500)))
    );
  }
}
