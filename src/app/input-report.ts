export interface InputReport{
    artist:String;
    title:String;
    isrc:String;
    duration:String;
}