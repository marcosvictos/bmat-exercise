import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { InputReport } from './input-report';
import { DatabaseService } from './database.service';
import { Subject, Observable, of } from 'rxjs';
import {
  map,
  debounceTime,
  distinctUntilChanged,
  switchMap,
  tap
} from 'rxjs/operators';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  @ViewChild('reportReader') reportReader: any;
  searchValue: any;
  title = 'bmat-exercise';
  records: any[] = [];
  savedRecords: any[] = [];
  matches: any[] = [];
  selectedRecord: InputReport;
  displayedColumns: string[] = ['artist', 'title', 'isrc', 'duration', 'actions'];
  results$: Observable<any>;
  isLoading: Boolean = false;
  insertLoading:Boolean = false;
  subject = new Subject();

  constructor(private databaseService: DatabaseService,private _snackBar: MatSnackBar) {
    this.getSavedRecords();

  }

  ngOnInit() {
    var self = this;
    this.results$ = this.subject.pipe(
      tap(() => (this.isLoading = true)),
      debounceTime(500),
      distinctUntilChanged(),
      switchMap((searchText: String) => this.fetchItems(searchText)),
      tap(() => (this.isLoading = false))
    );
  }

  triggerUpload() {
    this.reportReader.nativeElement.click();
  }

  fetchItems(searchInput: any) {
    var self = this;
    return this.databaseService.findAll().pipe(
      map((data: any) => {
        let records = data.split(/\r\n|\n/);
        let headers = self.getHeaders(records);
        let transformedData = self.getDataRecords(records, headers.length);
        return transformedData.filter((value: InputReport) => {

          if (typeof searchInput == 'string') {
            // On typing on the search button
            // Limit the search to only artist and title
            return (value.artist).toLowerCase().includes(<string>searchInput.toLowerCase()) || (value.title).toLowerCase().includes(<string>searchInput.toLowerCase());
          } else {
            // on click of the match button
            self.selectedRecord = searchInput;
            let firstMatcher = value.artist.includes(<string>searchInput.artist) || value.title.includes(<string>searchInput.title);
            let secondMatcher = searchInput.isrc != "" ? value.isrc == searchInput.isrc : undefined;
            if (typeof secondMatcher != 'undefined') {
              return firstMatcher || secondMatcher;
            }
            return firstMatcher;
          }

        });
      })
    );
  }
  searchItems() {
    let searchText: String = this.searchValue;
    console.log(searchText);
    this.subject.next(searchText);
  }

  findMatchingRecords(data:any){
    this.subject.next(data);
  }

  uploadReport(event: any): void {
    console.log(event.srcElement.files);
    let files = event.srcElement.files;

    // check if it is a valid csv
    if (this.isValidCsv(files[0])) {

      let input = event.target;
      let reader = new FileReader();
      reader.readAsText(input.files[0]);

      reader.onload = () => {
        let reportData = reader.result;
        let reportRecordsArray = (<string>reportData).split(/\r\n|\n/);

        let headersRow = this.getHeaders(reportRecordsArray);

        this.records = this.getDataRecords(reportRecordsArray, headersRow.length);
      };

      reader.onerror = function () {
        console.log('error is occured while reading file!');
      };

    } else {
      console.log("Invalid file");
      this.reset();
    }
  }
  // Check if uploaded file is a csv
  isValidCsv(file: any): Boolean {
    return file.name.endsWith('.csv');
  }
  // Method to get the headers
  getHeaders(recordsArray: any) {
    let headers = (<string>recordsArray[0]).split(',');
    let headerArray = [];
    for (let j = 0; j < headers.length; j++) {
      headerArray.push(headers[j]);
    }
    return headerArray;
  }
  // Method to populate the input report records
  getDataRecords(csvRecordsArray: any, headerLength: any) {
    let reportArr = [];

    for (let i = 1; i < csvRecordsArray.length; i++) {
      let currentRecord = (<string>csvRecordsArray[i]).split(',');
      // check if row is not empty and push the object into the records array.
      if (currentRecord.length == headerLength) {
        let inputReport: InputReport = {
          artist: currentRecord[0].trim(),
          title: currentRecord[1].trim(),
          isrc: currentRecord[2].trim(),
          duration: currentRecord[3].trim()
        };
        reportArr.push(inputReport);
      }
    }
    return reportArr;
  }

  // Get records saved in database
  getSavedRecords() {
    var self = this;
    this.databaseService.findAll().subscribe((data) => {
      let records = data.split(/\r\n|\n/);
      let headers = self.getHeaders(records);
      let transformedData = self.getDataRecords(records, headers.length);
      console.log(transformedData);
      self.savedRecords = transformedData;
    });
  }

  selectRecord() {
    console.log(this.selectedRecord);
    var self = this;
    // Arbitrary deletion from the database by removing the selected record from list
    // Different implementation with a real database
    this.records = this.records.filter((value: InputReport) => {
      return value != self.selectedRecord;
    });

    this._snackBar.open("Record selected successfully!");
  }

  insertRecord(data:any){
    var self = this;
    this.insertLoading = true;
    this.databaseService.insert(data).subscribe(res=>{
      console.log(res);
      this.insertLoading = false;
      self._snackBar.open("Record Inserted successfully!");
    })
  }

  // Method to clear the upload input field
  reset() {
    this.reportReader.nativeElement.value = "";
    this.records = [];
  }
}
